cas auto authinfo="/home/sas/.authinfo" host='sasviyahost.ec2.internal' port=5570 sessopts=(caslib=casuser timeout=1800 locale="en_US");
caslib _ALL_ assign;

/*Proc upload com astore, file -> caslib*/
proc astore;
    upload rstore=public.astore_gb_NEXUS
             store="/tmp/astore-1.0.0.sas";
quit;

/*Scorando com Astore*/
proc astore;
score data=public.HMEQ_TRATADA out=public.HMEQ_SCORED_ASTORE
rstore=public.astore_gb_NEXUS;
quit;

